 $(function(){
    //Peticion del listado de los paises y mostrar el select
    $.ajax({
        url:'https://coronavirus-19-api.herokuapp.com/countries',
        type:'GET',
        cache:false,
        dataType:"json",
        success:function(data){
            // autocomplete
              paises=[];//array
             //recorrer el data y hacer push al array paises
            for(var i in data){
                paises.push(data[i]["country"]);
            }
            autorrellenar();
        }
    });//fin de la peticion ajax 
    function autorrellenar(){
       $("#paises").autocomplete({
                source:paises,
                minLength:1,
                select: function(event,ui){
                  id=ui.item.label;
                  //hacemos la peticion ajax mediante el valor id
                  $.ajax({
                      url: 'https://coronavirus-19-api.herokuapp.com/countries/'+id,
                      type: "GET",
                      dataType:"json",
                       cache:false,
                      beforeSend:function(){
                        $('#loading').html("cargando...");
                      },
                      success:function(data){
                         $('#loading').empty();
                        var  paisnombre=data.country;
                         var arreglo={
                         "Fallecidos:":data.deaths,
                         "Facellidos este dia:":data.todayDeaths,
                         "Casos:":data.cases,
                         "Casos Activos:":data.active,
                         "Nuevos casos:":data.todayCases,
                         "Recuperados:":data.recovered,
                         "Estados Criticos:":data.critical
                         };
                         html="";
                          for(var indice in arreglo){
                            if(arreglo[indice]!=null){
                              html+="<strong>"+indice+"</strong><span id='texto'>"+arreglo[indice]+"</span><br/>";
                            }
                          }
                          $("#info").html(`
                                <div class="card border-light mb-3 mx-auto" style="max-width: 18rem;">
                                <div class="card-header"><h2>${paisnombre}</h2></div>
                                <div class="card-body mx-auto">
                                  ${html}
                                </div>
                                  </div>
                            `);
                          //console.log(html);
                      }
                  });
                }//fin select 
            });//fin autocomplete
    }//end function autorellenar()

    //cuando se envia algo en el form
    $('#formulario').submit(function(event) {
      /* Act on the event */
      event.preventDefault();
      /*Desactivamos el boton submit mientras se envia la peticion ajax*/
      $("#enviar").attr("disabled", true);
      //valor del input
      id=$('#paises').val();
      //validamos que se haya ingresado algo en el input y que no sea numero
      if (id == null || id.length == 0 || /^\s+$/.test(id) || !isNaN(id) ) {
        alert('ingrese un pais');
        $("#enviar").attr("disabled", false);
      }
      else{ //en caso de que si se ingresa 
        $.ajax({
                url: 'https://coronavirus-19-api.herokuapp.com/countries/'+id,
                type: "GET",
                 cache:false,
                beforeSend:function(){
                  $('#loading').html("cargando...");

                },
                success:function(data){
                  $('#loading').empty();
                  var  paisnombre=data.country;
                   html="";
                  if (typeof(data)==='object') {
                    var arreglo={
                     "Fallecidos:":data.deaths,
                     "Facellidos este dia:":data.todayDeaths,
                     "Casos:":data.cases,
                     "Casos Activos:":data.active,
                     "Nuevos casos:":data.todayCases,
                     "Recuperados:":data.recovered,
                     "Estados Criticos:":data.critical
                   };
                  
                    for(var indice in arreglo){
                      if(arreglo[indice]!=null){
                        html+="<strong>"+indice+"</strong><span id='texto'>"+arreglo[indice]+"</span><br/>";
                      }
                    }
                  }
                  else{
                    html+="Country not Found";
                  }
                 $("#info").html(`
                                <div class="card border-light mb-3 mx-auto" style="max-width: 18rem;">
                                <div class="card-header"><h2>${!paisnombre==0 ? paisnombre:"" }</h2></div>
                                <div class="card-body mx-auto">
                                  ${html}
                                </div>
                                </div>
                            `);
                },
                complete: function(response){
                  $("#enviar").attr("disabled", false);
                }
            });//fin de la peticion ajax
         }
    });  
 });
